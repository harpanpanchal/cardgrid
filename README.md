This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installation guideline

1. Install the application by running 'npm install' in the command prompt / terminal.

2. Once installed, type 'npm start' in the command prompt. You can then view the application in the browser by opening http://localhost:9000 url.

## Technical specifications

1. All the main resource folders / files are located within 'src' folder. Some of them are Actions / Reducers / components / apis, etc.

2. Data has been fetched via API and displayed using React / Redux / Thunk as a middleware.

3. CSS / Flexbox has been used for styling and responsiveness.

4. Separate components created mainly for ItemListing / SingleItem / ArrowIcon / Pdfdocument. They can be found inside the 'components' folder. ItemListing component fetches the single item code to avoid duplicate code / DRY.

5. SASS (CSS Pre-processor) / Webpack (Bundle Moduler) implemented as well.
