const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const SRC_DIR = path.resolve(__dirname, 'src');
const DIST_DIR = path.resolve(__dirname, 'dist');


module.exports = {
    optimization: {
        minimizer: [
            // we specify a custom UglifyJsPlugin here to get source maps in production
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                uglifyOptions: {
                    compress: true,
                    ecma: 6,
                    mangle: true
                },
                sourceMap: true
            })
        ]
    },
    devtool: 'cheap-module-eval-source-map',
    entry: ["babel-polyfill", SRC_DIR + '/index.js'],
    output: {
        path: DIST_DIR,
        filename: 'bundle.js',
        chunkFilename: '[id].js',
        publicPath: ''
    },
    module: {
        rules: [
            { test: /\.ts?$/, exclude: [/(node_modules|bower_components)/], loader: 'ts-loader' },
            { test: /\.(js|jsx)$/, exclude: /(node_modules|bower_components)/, use: ['babel-loader', 'eslint-loader'] },
            { test: /\.json$/, loader: 'json-loader' },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { url: false, sourceMap: false } },
                    { loader: 'sass-loader', options: { sourceMap: false } }
                ]
            },

            {
                test: /\.(eot|otf|woff|ttf|svg)(\?\S*)?$/,
                loader: 'url-loader?name=assets/fonts/[name].[hash].[ext]'
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader",
                        options: { minimize: true }
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.jsx', '.js', '.json', '.scss', '.eot', '.ttf', '.svg', '.woff']
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: SRC_DIR + '/index.html',
            filename: 'index.html',
            inject: 'body'
        }),
        new MiniCssExtractPlugin({
            //filename: devMode ? '[name].css' : '[name].[hash].css',
            //chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new webpack.ProvidePlugin({
            "$": 'jquery',
            "jQuery": 'jquery',
            'window.jQuery': 'jquery',
            "Popper": ['popper.js', 'default']
        }),
        new webpack.NamedModulesPlugin()
    ],
    devServer: {
        contentBase: SRC_DIR,
        hot: true,
        open: true,
        historyApiFallback: true,
        port: 9000
    }
};
