import React from 'react';
import PropTypes from "prop-types";

const Pdfdocument = (props) => {
        return (<div className="pdfDoc"><span className="pdfIcon" /> <span className="docSize">PDF ({props.item.documentSize})</span></div>);
};
Pdfdocument.propTypes = {
    item: PropTypes.object
  };

export default Pdfdocument;