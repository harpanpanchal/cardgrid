import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchList } from "../actions";
import SingleItem from './SingleItem';

class ItemListing extends React.Component {
   componentDidMount() {
    this.props.fetchList();
  }
  renderList() {
    if (this.props.itemlist) {
      return this.props.itemlist.map(item => {
        return (<SingleItem item={item} key={item.id} />)
    })
    }
}
render() {
  return (this.renderList())
}
};

const mapStateToProps = state => {
return {
    itemlist: state
  };
};
ItemListing.propTypes = {
  fetchList: PropTypes.func.isRequired,
  itemlist: PropTypes.array,
  item: PropTypes.object
};

export default connect(
  mapStateToProps,
  { fetchList }
)(ItemListing);