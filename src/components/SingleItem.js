import React from 'react';
import PropTypes from "prop-types";
import ArrowIcon from './ArrowIcon';
import Pdfdocument from './Pdfdocument';

const SingleItem = (props) => {
return (<React.Fragment>
    <div className={"item-" + props.item.id} featured={props.item.featured ? props.item.featured : ""}>
        <a href={props.item.link} className="card" target="_blank" title="Opens in a new window">
          <article>
            <h1>{props.item.title}</h1>
          {(props.item.description) ? <span className="description">{props.item.description}</span> : null}
        </article>
{(props.item.documentSize) ? <Pdfdocument item={props.item} /> : ""}
<ArrowIcon item={props.item} />
</a>
</div>
</React.Fragment>)
};

SingleItem.propTypes = {
  item: PropTypes.object
};

export default SingleItem;