import React from 'react';
import PropTypes from "prop-types";

const ArrowIcon = (props) => {
    if (props.item.featured === "true" && !props.item.documentSize)
    {
        return <span className="arrow featured-icon-right" />;
    }
    if (props.item.documentSize)
    {
        return <span className="arrow down" />
    }
    if (props.item.featured === "false" && !props.item.documentSize ) {
        return <span className="arrow right" />    
        }
};
ArrowIcon.propTypes = {
    item: PropTypes.object
};

export default ArrowIcon;