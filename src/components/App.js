import React, { Component } from "react";
import ItemListing from "./ItemListing";
import "../styles/index.scss";
class App extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="band">
          <ItemListing />
        </div>
      </React.Fragment>
    );
  }
}

export default App;
