import fetchJsonApi from "../apis/fetchJsonApi";

export const fetchList = () => async dispatch => {
  const response = await fetchJsonApi.get("/fed-test/items.json");
    dispatch({
    type: "FETCH_LIST",
    payload: response.data.items
  });
};
