const itemlistReducer = (state = [], action) => {
  switch (action.type) {
    case "FETCH_LIST":
      return action.payload;
    default:
      return state;
  }
};

export default itemlistReducer;