import React from "react";
import ReactDOM from "react-dom";
import "babel-polyfill";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import App from "./components/App";
import itemlistReducer from "./reducers";
const store = createStore(itemlistReducer, applyMiddleware(thunk));
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector("#root")
);
if (module.hot) {
  module.hot.accept();
}